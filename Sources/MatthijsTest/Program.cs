﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZMQ;
using Exception = System.Exception;
using System.Threading;

namespace MatthijsTest
{
    class Program
    {
        // context is some static state thingy: it's also the medium with which the inproc transport works. to use 0MQ, you need a context
        private static Context mContext = new Context();

        private const string ServiceAddress = "inproc://service";

        static void Main(string[] args)
        {
            try
            {
                new Thread(ServiceMethod).Start();
                Thread.Sleep(10); // most of the time, you can connect before the bind has finished. this is nto the case with the inproc transport
                using(var xClient = mContext.Socket(SocketType.REQ))
                {
                    // connect to the service. which end of the request/reply sequence connects and which binds does not matter. 
                    // this allows interesting things like load-balancing etc
                    xClient.Connect(ServiceAddress);
                    xClient.Send("Hello, World!", Encoding.Unicode);
                    Console.WriteLine("Output: '{0}'", xClient.Recv(Encoding.Unicode));
                    xClient.Send("stop", Encoding.Unicode);
                    xClient.RecvAll();
                }
            }finally
            {
                mContext.Dispose();
            }
        }

        private static void ServiceMethod()
        {
            using(var xSocket = mContext.Socket(SocketType.REP))
            {
                xSocket.Bind(ServiceAddress);
                var xPoll = new PollItem[]
                            {
                                xSocket.CreatePollItem(IOMultiPlex.POLLIN)
                            };
                var xRun = true;
                xPoll[0].PollInHandler += delegate(Socket socket, IOMultiPlex revents)
                                          {
                                              var xMsg = xSocket.Recv(Encoding.Unicode);
                                              if(xMsg == "stop")
                                              {
                                                  xRun = false;
                                              }
                                              xSocket.Send(new String(xMsg.Reverse().ToArray()), Encoding.Unicode);
                                          };
                while(xRun)
                {
                    mContext.Poll(xPoll, 1000000);
                }
            }
        }
    }
}
